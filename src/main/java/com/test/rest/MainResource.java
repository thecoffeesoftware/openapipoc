package com.test.rest;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("main")
@Tag(name = "main")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MainResource {

	@Inject
	private SubResource subResource;

	@Path("subresurce")
	@Operation
	public SubResource getSubResource(){
		return subResource;
	}

	@GET
	@Operation(operationId = "getMain")
	@RequestBody(content = @Content(schema = @Schema(implementation = MainDTO.class)))
	public Response getMain(MainDTO dtoBody){
		MainDTO dto = new MainDTO();
		dto.setName("mainName");
		dto.setNumber(1);
		dto.setValue("mainValue");
		return Response.ok(dto).build();
	}
}
