package com.test.rest;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.core.Response;

@Stateless
public class SubResource {

	@GET
	@Operation(operationId = "getTest")
	@RequestBody(content = @Content(schema = @Schema(implementation = TestDTO.class)))
	public Response getMain(TestDTO dtoBody){
		TestDTO dto = new TestDTO();
		dto.setName("mainName");
		dto.setNumber(1);
		dto.setValue("mainValue");
		return Response.ok(dto).build();
	}

}
