package com.test.rest;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.ApplicationPath;

@OpenAPIDefinition(
		info = @Info(
				title = "some API",
				version = "1.0.0",
				description = "some API"
		),
		tags = {
				@Tag(name = "main", description = "main related operations"),
		},
		servers = {
				@Server(url = "http://localhost:8080/rest", description = "Local server"),
		}
)
@ApplicationPath("rest")
public class ApplicationResource {
}
