package com.test.openapi;

import io.smallrye.openapi.api.OpenApiConfig;
import io.smallrye.openapi.api.OpenApiConfigImpl;
import io.smallrye.openapi.api.OpenApiDocument;
import io.smallrye.openapi.runtime.OpenApiProcessor;
import io.smallrye.openapi.runtime.io.OpenApiSerializer;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.jboss.jandex.Index;
import org.jboss.jandex.IndexReader;
import org.jboss.jandex.IndexWriter;
import org.jboss.jandex.Indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class OfflineOpenApi {

	private static final Logger LOG = Logger.getLogger(OfflineOpenApi.class.getName());
	private static final OpenApiSerializer.Format FORMAT = OpenApiSerializer.Format.YAML;
	private static final String destinationDir = "api/";
	private static final String jandexPath = destinationDir + "jandex-index.idx";
	private static final String webPackageDir = "target/classes/com/test/rest";

	public static void main(String[] args) throws IOException {
		createAndPersistJandexIndexes();
		OpenApiDocument.INSTANCE.reset();
		OpenApiConfig openApiConfig = getConfig();
		OpenApiDocument.INSTANCE.config(openApiConfig);
		OpenAPI api = getFromJandex();
		OpenApiDocument.INSTANCE.modelFromAnnotations(api);
		OpenApiDocument.INSTANCE.filter(OpenApiProcessor.getFilter(openApiConfig, OfflineOpenApi.class.getClassLoader()));
		OpenApiDocument.INSTANCE.initialize();

		String openapi = OpenApiSerializer.serialize(OpenApiDocument.INSTANCE.get(), FORMAT);

		Path path = Paths.get(destinationDir);
		Files.createDirectories(path);
		String fileName = FORMAT == OpenApiSerializer.Format.JSON ? "openapi.json" : "openapi.yml";
		Files.writeString(path.resolve(fileName), openapi);
	}

	private static void createAndPersistJandexIndexes() throws IOException {
		Indexer indexer = new Indexer();
		List<File> filesInPackage = getPackage(webPackageDir);
		filesInPackage.stream()
				.filter(file -> file.getName().endsWith(".class"))
				.collect(Collectors.toSet()).forEach(file -> {
			try {
				InputStream stream = new FileInputStream(file);
				try (stream) {
					indexer.index(stream);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		});
		Index index = indexer.complete();
		new File(destinationDir).mkdirs();
		File indexFile = new File(jandexPath);
		indexFile.createNewFile();
		FileOutputStream out = new FileOutputStream(indexFile);
		try (out) {
			IndexWriter writer = new IndexWriter(out);
			writer.write(index);
		}
	}

	private static List<File> getPackage(String packageName) {
		List<File> files = new ArrayList<>();
		listf(packageName, files);
		return files;
	}

	private static void listf(String directoryName, List<File> files) {
		File directory = new File(directoryName);
		// Get all files from a directory.
		File[] fList = directory.listFiles();
		if (fList != null)
			for (File file : fList) {
				if (file.isFile()) {
					files.add(file);
				} else if (file.isDirectory()) {
					listf(file.getAbsolutePath(), files);
				}
			}
	}

	private static OpenAPI getFromJandex() {
		try (InputStream jandexIndexStream = new FileInputStream(jandexPath)) {
			if (jandexIndexStream.available() == 0) {
				LOG.log(Level.WARNING, "Jandex index file is not present, OpenApi model won't be read from annotations");
				return null;
			}

			IndexReader reader = new IndexReader(jandexIndexStream);
			Index index = reader.read();
			return OpenApiProcessor.modelFromAnnotations(getConfig(), index);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Failed to read Jandex index file", e);
			return null;
		}
	}

	private static OpenApiConfig getConfig() {
		return new OpenApiConfig() {
			@Override
			public String modelReader() {
				return null;
			}

			@Override
			public String filter() {
				return null;
			}

			@Override
			public boolean scanDisable() {
				return false;
			}

			@Override
			public Set<String> scanPackages() {
				return new HashSet<>();
			}

			@Override
			public Set<String> scanClasses() {
				return new HashSet<>();
			}

			@Override
			public Set<String> scanExcludePackages() {
				return new HashSet<>();
			}

			@Override
			public Set<String> scanExcludeClasses() {
				return new HashSet<>();
			}

			@Override
			public Set<String> servers() {
				return null;
			}

			@Override
			public Set<String> pathServers(String path) {
				return null;
			}

			@Override
			public Set<String> operationServers(String operationId) {
				return null;
			}

			@Override
			public boolean scanDependenciesDisable() {
				return false;
			}

			@Override
			public Set<String> scanDependenciesJars() {
				return null;
			}

			@Override
			public boolean schemaReferencesEnable() {
				return false;
			}

			@Override
			public String customSchemaRegistryClass() {
				return null;
			}
		};
	}

}
