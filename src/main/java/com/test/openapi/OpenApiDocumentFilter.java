package com.test.openapi;

import org.eclipse.microprofile.openapi.OASFactory;
import org.eclipse.microprofile.openapi.OASFilter;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.eclipse.microprofile.openapi.models.Operation;
import org.eclipse.microprofile.openapi.models.PathItem;
import org.eclipse.microprofile.openapi.models.media.Schema;
import org.eclipse.microprofile.openapi.models.responses.APIResponse;
import org.eclipse.microprofile.openapi.models.tags.Tag;

import java.util.Arrays;
import java.util.List;

public class OpenApiDocumentFilter implements OASFilter {
	public static final String IGNORE_TAG = "ignore";

	@Override
	public Operation filterOperation(Operation operation) {
		if (operation.getTags() != null && operation.getTags().contains(IGNORE_TAG)) {
			return null;
		}
		addErrorResponse(operation, "400", "Bad request");
		addErrorResponse(operation, "401", "Authentication failed");
		addErrorResponse(operation, "404", "Entity not found");
		addErrorResponse(operation, "500", "Internal server error");
		return operation;
	}

	@Override
	public PathItem filterPathItem(PathItem pathItem) {
		List<Operation> operations = Arrays.asList(
				pathItem.getGET(),
				pathItem.getPOST(),
				pathItem.getPUT(),
				pathItem.getDELETE(),
				pathItem.getPATCH(),
				pathItem.getHEAD(),
				pathItem.getTRACE(),
				pathItem.getOPTIONS()
		);

		boolean noOperationDefined = true;
		for (Operation operation : operations) {
			if (operation != null) {
				noOperationDefined = false;
				break;
			}
		}
		return noOperationDefined ? null : pathItem;
	}

	@Override
	public void filterOpenAPI(OpenAPI openAPI) {
		if(openAPI.getComponents() != null) {
			openAPI.getComponents().addSchema("ErrorResponse", OASFactory.createSchema()
					.addProperty("errors", OASFactory.createSchema()
							.type(Schema.SchemaType.ARRAY)
							.items(OASFactory.createSchema().type(Schema.SchemaType.OBJECT)
									.addProperty("actual", OASFactory.createSchema().type(Schema.SchemaType.OBJECT))
									.addProperty("code", OASFactory.createSchema().type(Schema.SchemaType.STRING))
									.addProperty("expected", OASFactory.createSchema().type(Schema.SchemaType.OBJECT))
									.addProperty("field", OASFactory.createSchema().type(Schema.SchemaType.STRING))
									.addProperty("msg", OASFactory.createSchema().type(Schema.SchemaType.STRING))))
			);
		}
	}

	@Override
	public Tag filterTag(Tag tag) {
		return IGNORE_TAG.equals(tag.getName()) ? null : tag;
	}

	private void addErrorResponse(Operation operation, String code, String description) {
		if (!operation.getResponses().hasAPIResponse(code)) {
			Schema schema = OASFactory.createSchema().ref("#/components/schemas/ErrorResponse");
			APIResponse response = OASFactory.createAPIResponse()
					.description(description)
					.content(OASFactory.createContent()
							.addMediaType("application/json", OASFactory.createMediaType().schema(schema)));
			operation.getResponses().addAPIResponse(code, response);
		}
	}
}
